import dotenv from "dotenv";
import express, {Request, Response} from "express";
import {Routes} from "./routes/routes";
import "reflect-metadata";
import {AppDataSource} from "./data-source";

dotenv.config();
const routePrefix = process.env.API_PREFIX || "/api";
const cors = require("cors");

AppDataSource.initialize().then(async connection => {
    const app = express();
    app.use(cors());

    app.get("/", (req: Request, res: Response) => {
        res.send("Hello World!");
    });

    Routes.forEach(route => {
        app.use(express.json());
        (app as any)[route.method](routePrefix.concat(route.route), (req: Request, res: Response, next: Function) => {
            const result = (new (route.controller as any))[route.action](req, res, next);
            if (result instanceof Promise) {
                result.then(r => r !== null && r !== undefined ? res.send(r) : undefined);
            } else if (result !== null && result !== undefined) {
                res.json(result);
            }
        });
    });
        const port = process.env.PORT || 3000;
        app.listen(port);
        console.log(`Server started on port ${port} - http://localhost:${port}/`);

}).catch(error => console.log(error));

export default AppDataSource;