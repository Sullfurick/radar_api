import {RadarController} from "../controller/RadarController";

export const Routes = [
    {
        method: "get",
        route: "/radar",
        controller: RadarController,
        action: "getRadar"
    },
    {
        method: "post",
        route: "/radar",
        controller: RadarController,
        action: "createRadar"
    },
    {
        method: "patch",
        route: "/radar",
        controller: RadarController,
        action: "updateRadarById"
    },
    {
        method: "get",
        route: "/radar/all",
        controller: RadarController,
        action: "getAllRadar"
    },
    {
        method: "get",
        route: "/importCSV",
        controller: RadarController,
        action: "importCSVToDatabase"
    }
];