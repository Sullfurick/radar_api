import "reflect-metadata"
import { DataSource } from "typeorm"
import {Radar} from "./entity/Radar";

export const AppDataSource = new DataSource({
    type: "mysql",
    host: "localhost",
    port: 3306,
    username: "root",
    password: "",
    database: "radar",
    synchronize: true,
    logging: false,
    entities: [Radar],
    migrations: [],
    subscribers: [],
})
