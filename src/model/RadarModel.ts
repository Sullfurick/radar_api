import {Radar} from "../entity/Radar";
import {AppDataSource} from "../data-source";
import {UpdateResult} from "typeorm";
import fs from "fs";
import csvParser from "csv-parser";
import { ParsedQs } from "qs";


export class RadarModel {
    private RadarRepository = AppDataSource.getRepository(Radar)

    constructor() {
    }

    async getRadarById(id: string): Promise<Radar> {
        try {
            return await this.RadarRepository.findOneBy({id: id});
        } catch (error) {
            throw error;
        }
    }

    async createRadar(equipement: string): Promise<Radar> {
        try {
            const data = {
                equipement: equipement
            }
            // console.log(data)
            const created_data = this.RadarRepository.create(data);
            const query = await this.RadarRepository.save(created_data);
            console.log(query)
            return query
        } catch (error) {
            throw error;
        }
    }

    async patchTodoById(id: string, equipement: string): Promise<UpdateResult> {
        try {
            const data = {
                equipement: equipement,
            }
            return this.RadarRepository.update(id, data);
        } catch (error) {
            throw error;
        }
    }

    async getAllRadar(): Promise<Radar[]> {
        try {
            return await this.RadarRepository.find();
        } catch (error) {
            throw error;
        }
    }

    async createRadarFromCSV(row: any): Promise<Radar> {
        try {
            const data = {
                date_heure_dernier_changement: row?.date_heure_dernier_changement,
                date_heure_creation: row?.date_heure_creation,
                departement: row?.departement,
                latitude: parseFloat(row?.latitude),
                longitude: parseFloat(row?.longitude),
                direction: row?.direction,
                equipement: row?.equipement,
                date_installation: row?.date_installation,
                type: row?.type,
                emplacement: row?.emplacement,
                route: row?.route,
                longueur_troncon_km: parseFloat(row?.longueur_troncon_km) || null,
                vitesse_poids_lourds_kmh: parseFloat(row?.vitesse_poids_lourds_kmh) || null,
                vitesse_vehicules_legers_kmh: parseFloat(row?.vitesse_vehicules_legers_kmh) || null,
            };
            const created_data = this.RadarRepository.create(data);
            const query = await this.RadarRepository.save(created_data);
            return query;
        } catch (error) {
            throw error;
        }
    }

    async importCSVToDatabase(csvFilePath: string) {
    fs.createReadStream(csvFilePath)
        .pipe(csvParser())
        .on('data', async (row) => {
            try {
                console.log("Row" + row.route + row.latitude + row.longitude)
                await this.createRadarFromCSV(row);
            } catch (error) {
                console.error(`Erreur lors de l'insertion : ${error.message}`);
            }
        })
        .on('end', () => {
            console.log('Import terminé.');
        });
}

}