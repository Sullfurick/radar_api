import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class Radar {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({ nullable: true })
  date_heure_dernier_changement: string;

  @Column({ nullable: true })
  date_heure_creation: string;

  @Column({ nullable: true })
  departement: string;

  @Column('decimal', { precision: 20, scale: 6 })
  latitude: number;

  @Column('decimal', { precision: 20, scale: 6 })
  longitude: number;

  @Column({ nullable: true })
  direction: string;

  @Column({ nullable: true })
  equipement: string;

  @Column({ nullable: true })
  date_installation: string;

  @Column({ nullable: true })
  type: string;

  @Column({ nullable: true })
  emplacement: string;

  @Column({ nullable: true })
  route: string;

  @Column({ nullable: true })
  longueur_troncon_km: number;

  @Column({ nullable: true })
  vitesse_poids_lourds_kmh: number;

  @Column({ nullable: true })
  vitesse_vehicules_legers_kmh: number;
}
