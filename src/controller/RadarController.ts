import {RadarModel} from "../model/RadarModel";
import {NextFunction, Request, Response} from "express";


export class RadarController {
    radarModel = new RadarModel();

    constructor() {
    }
    async getRadar(request: Request, _res: Response, next: NextFunction) {
        try {
            return await this.radarModel.getRadarById(<string>request.query.id);
        } catch (error) {
            next(error);
        }
    }

    async createRadar(request: Request, _res: Response, next: NextFunction) {
        try {
            return await this.radarModel.createRadar(request.body.equipement);
        } catch (error) {
            next(error)
        }
    }

    async updateRadarById (request: Request, _res: Response, next: NextFunction) {
        try {
            const {id, equipement} = request.body;
            return await this.radarModel.patchTodoById(
                id,
                equipement,
            );
        } catch (error) {
            next(error);
        }
    }

    async getAllRadar (request: Request, _res: Response, next: NextFunction) {
        try {
            return await this.radarModel.getAllRadar();
        } catch (error) {
            next(error);
        }
    }

    async importCSVToDatabase (request: Request, _res: Response, next: NextFunction) {
        try {
            console.log(request.query.path);
            return await this.radarModel.importCSVToDatabase(<string>request.query.path);
        } catch (error) {
            next(error);
        }
    }
}